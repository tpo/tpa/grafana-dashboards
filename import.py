#!/usr/bin/python3
# coding: utf-8

"""import downloaded dashboard in the right place"""
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging
import os.path
import re
import shutil
import sys


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__, epilog="""""")
    parser.add_argument(
        "--quiet",
        "-q",
        dest="log_level",
        action="store_const",
        const="warning",
        default="info",
    )
    parser.add_argument(
        "--debug",
        "-d",
        dest="log_level",
        action="store_const",
        const="debug",
        default="info",
    )
    parser.add_argument(
        "--file",
        default=None,
        help="import given file directly",
    )
    parser.add_argument(
        "--download-directory",
        default=os.path.expanduser("~/Downloads"),
        help="download directory to inspect, default: %(default)s",
    )
    parser.add_argument(
        "--base-directory",
        default=".",
        help="base directory for the folder, default: %(default)s",
    )
    parser.add_argument(
        "--folder", "-f", default=None, help="folder path, default: guess"
    )
    parser.add_argument(
        "--dryrun",
        "-n",
        action="store_true",
        help="do nothing",
    )
    return parser.parse_args(args=args)


def guess_dir(folder, guessed_name):
    for root, dirs, files in os.walk(folder):
        for name in files:
            if name == guessed_name:
                yield root


def guess_folder(path, base_directory, folder=None):
    name = os.path.basename(path)
    m = re.search(r"(?P<name>.*?)-(?P<date>\d+).json", name)
    if not m:
        logging.debug("skipping %s not matching pattern", path)
        return
    guessed_name = m.group("name").lower().replace(" ", "-") + ".json"
    if folder:
        return os.path.join(folder, guessed_name)
    guessed_location = list(guess_dir(base_directory, guessed_name))
    if not guessed_location:
        logging.error(
            "cannot guess folder for file %s (%s), skipping: use --folder to select folder",  # noqa: E501
            path,
            guessed_name,
        )
        return
    if len(guessed_location) > 1:
        logging.warning(
            "more than one possible locations found, picking first one: %s",
            guessed_location,
        )
    guessed_location = guessed_location.pop()
    return os.path.join(guessed_location, guessed_name)


def main(args):
    if args.file:
        path = args.file
        folder_path = guess_folder(path, args.base_directory, args.folder)
        if not folder_path:
            logging.error(
                "couldn't guess folder_path. If you provide --file, ensure "
                "the file match foo-date.json"
            )
            sys.exit(1)
        if args.dryrun:
            logging.info("would move %s to %s", path, folder_path)
        else:
            logging.info("moving %s to %s", path, folder_path)
            try:
                shutil.move(path, folder_path)
            except FileNotFoundError:
                logging.error(
                    "ensure the folder directory %s exists", args.folder
                )
                sys.exit(1)

    for root, dirs, files in os.walk(args.download_directory):
        for name in sorted(files + dirs):
            path = os.path.join(root, name)
            if not path.endswith(".json"):
                logging.debug("skipping %s", path)
                continue
            folder_path = guess_folder(path, args.base_directory, args.folder)
            if not folder_path:
                continue
            if args.dryrun:
                logging.info("would move %s to %s", path, folder_path)
            else:
                logging.info("moving %s to %s", path, folder_path)
                shutil.move(path, folder_path)


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(format="%(message)s", level=args.log_level.upper())
    main(args)
