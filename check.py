#!/usr/bin/python3
# coding: utf-8

"""verify all Grafana dashboards are correct"""
# Copyright (C) 2023 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import json
import logging
import os
import os.path
import sys
from typing import AnyStr, Union


epilog = """For now only check if dashboards parse as JSON and have
the right tag."""


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__, epilog=epilog)
    parser.add_argument(
        "--verbose",
        "-v",
        dest="log_level",
        action="store_const",
        const="info",
        default="warning",
        help="show processed files and more verbose information",
    )
    parser.add_argument(
        "--debug",
        "-d",
        dest="log_level",
        action="store_const",
        const="debug",
        default="warning",
        help="show *all* files processed and debug information",
    )
    parser.add_argument(
        "--target", "-t", default=".", help="target path, default: %(default)s"
    )
    return parser.parse_args()


def check_file(path: Union[AnyStr, os.PathLike[AnyStr]]) -> bool:
    clean = True
    with open(path) as fp:
        try:
            data = json.load(fp)
        except json.JSONDecodeError as e:
            logging.error("failed to parse %s as JSON: %s", path, e)
            return False
        if "provisioned" not in data.get("tags", []):
            logging.warning(
                "dashboard %s missing 'provisioned' tag",
                path,
            )
            clean = False
    return clean


def main(target: Union[AnyStr, os.PathLike[AnyStr]]):
    clean = True
    for root, dirs, files in os.walk(target):
        for name in files + dirs:
            path = str(os.path.join(root, name))
            if not path.endswith(".json"):
                logging.debug("skipped %s", path)
                continue
            logging.info("checking %s", path)
            if not check_file(path):
                clean = False
    if not clean:
        logging.error("JSON not clean, exiting with errors")
        sys.exit(1)
    logging.info("no error found")


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(format="%(message)s", level=args.log_level.upper())
    main(args.target)
