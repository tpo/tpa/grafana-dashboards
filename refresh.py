#!/usr/bin/python3

"""Reimport upstream dashboards in our provisionning system.

This exists mostly because upstream Grafana cannot parse Dashboard
variables when provisionning:

https://github.com/grafana/grafana/issues/10786

Therefore we need to do pattern replacement ourselves. This also
serves as a canonical source of truth for where the dashboards come
from.


"""

import re

import requests


AUTO_DASHBOARD_MAP = {
    "services/minio-bucket.json": "https://raw.githubusercontent.com/minio/minio/master/docs/metrics/prometheus/grafana/minio-bucket.json",  # noqa: E501
    "services/minio-dashboard.json": "https://raw.githubusercontent.com/minio/minio/master/docs/metrics/prometheus/grafana/minio-dashboard.json",  # noqa: E501
    "services/apache.json": "https://grafana.com/api/dashboards/3894/revisions/5/download",  # noqa: E501
    "system/node-exporter-full.json": "https://grafana.com/api/dashboards/1860/revisions/13/download",  # noqa: E501
    "system/node-exporter-server-metrics.json": "https://grafana.com/api/dashboards/405/revisions/8/download",  # noqa: E501
}


MANUAL_DASHBOARD_MAP = {
    "postgresql.json": "https://grafana.com/api/dashboards/455/revisions/2/download",  # noqa: E501
    "prometheus-2.0-overview.json": "https://grafana.com/api/dashboards/3662/revisions/2/download",  # noqa: E501
    # local changes, no change upstream since at least 2023-11-24
    "meta/grafana-internals.json": "https://grafana.com/api/dashboards/3590/revisions/3/download",  # noqa: E501
    # local changes, no change upstream since at least 2023-11-24
    "services/bind.json": "https://grafana.com/api/dashboards/10024/revisions/2/download",  # noqa: E501
    # local changes, no change upstream since at least 2023-11-24
    "system/smartmon-textfile.json": "https://grafana.com/api/dashboards/3992/revisions/1/download",  # noqa: E501
}


def main():
    pattern = re.compile(rb"\${DS_[A-Z]+}")
    print("fetching dashboards...")
    for path, url in AUTO_DASHBOARD_MAP.items():
        print("%s -> %s" % (url, path))
        body = requests.get(url).content
        body = pattern.sub(b"Prometheus", body)
        with open(path, "wb") as fp:
            fp.write(body)
    print("the following dashboards were modified locally:")
    for path, url in MANUAL_DASHBOARD_MAP.items():
        print("%s -> %s" % (url, path))
    print("""To update, see README.md""")


if __name__ == "__main__":
    main()
